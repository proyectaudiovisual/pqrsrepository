package com.uniajc.pqrspro.pqrspro_project.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

@Data
@Entity
@Table(name = "usuario", schema = "pqrs")
public class Usuario {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "apellido", nullable = false)
    private String apellido;

    @Column(name = "correo", nullable = false)
    private String correo;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "rol", nullable = false)
    private int rol;

    public void generarPassword() throws NoSuchAlgorithmException {
        String[] symbols = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        int length = 10;
        Random random = SecureRandom.getInstanceStrong();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int indexRandom = random.nextInt(symbols.length);
            sb.append(symbols[indexRandom]);
        }
        String passwordtemp = sb.toString();
        this.password = passwordtemp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombreSet) {
        this.nombre = nombreSet;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellidoSet) {
        this.apellido = apellidoSet;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correoSet) {
        this.correo = correoSet;
    }

    public String getUser() {
        return username;
    }

    public void setUser(String userSet) {
        this.username = userSet;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwordSet) {
        this.password = passwordSet;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rolSet) {
        this.rol = rolSet;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id" + id +
                ", nombre'" + nombre + '\'' +
                ", apellido'" + apellido + '\'' +
                ", correo'" + correo + '\'' +
                ", user'" + username + '\'' +
                ", password'" + password + '\'' +
                ", rol'" + rol + '\'' +
                "}";
    }
}
