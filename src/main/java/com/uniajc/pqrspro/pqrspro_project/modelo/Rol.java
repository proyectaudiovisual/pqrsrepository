package com.uniajc.pqrspro.pqrspro_project.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "rol", schema = "pqrs")
public class Rol {

    @Id
    @Column(name = "idrol")
    private Long idrol;

    @Column(name = "nombrerol")
    private String nombrerol;

    public Long getIdrol() {
        return idrol;
    }

    public void setIdrol(Long idrol) {
        this.idrol = idrol;
    }

    public String getNombrerol() {
        return nombrerol;
    }

    public void setNombrerol(String nombrerol) {
        this.nombrerol = nombrerol;
    }
}
