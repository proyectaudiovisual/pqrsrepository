package com.uniajc.pqrspro.pqrspro_project.repository;

import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolRepository extends CrudRepository<Rol, Long> {
}
