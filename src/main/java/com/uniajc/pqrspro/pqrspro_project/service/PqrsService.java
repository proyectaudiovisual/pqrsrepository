package com.uniajc.pqrspro.pqrspro_project.service;
import com.uniajc.pqrspro.pqrspro_project.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;

import java.util.List;

public interface PqrsService {

    String registrar(PqrsDto pqrsDto);

    String actualizar(PqrsDto pqrsDto);

    String eliminar(Long id);

    Pqrs consultarPqrsById(Long id);

    List<Pqrs> consultarPqrs();
}
